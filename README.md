# Nuxt + TS + Eslint + Subdomain

Um modelo de projeto inicial do [Nuxt.js](https://github.com/nuxt/nuxt.js) com um ambiente personalizado para o desenvolvimento com Typescript e com ferramenta de Lint para verificar seu código de acordo com as regras Airbnb.

![main](https://gitlab.com/i.leonardo/template-nuxt-ts-subdomain/raw/master/main.png)

## Instalação

Este template é gerado através do Legacy API [@vue/cli-init](https://cli.vuejs.org/guide/creating-a-project.html#using-the-gui):

``` bash
# remote template
$ vue init gitlab:i.leonardo/template-nuxt-ts-subdomain my-project

# install dependencies
$ cd my-project
$ npm install
```

## Configurar novos Domínios

- Crie uma pasta em `src/pages/subdomain/` com o nome que será utilizado:

![location-folder](https://gitlab.com/i.leonardo/template-nuxt-ts-subdomain/raw/master/tutorial/1.png)

- Configure o arquivo `src/route.js` para adicionar as rotas de acordo com o **nome da pasta criada**:

![router.js](https://gitlab.com/i.leonardo/template-nuxt-ts-subdomain/raw/master/tutorial/2.png)

## Uso

### Development

``` bash
# build for development and launch the server
$ npm run dev
```

Acessa o site: [http://localhost:3000](http://localhost:3000)

### Production

``` bash
# build for production and launch the server
$ npm run build
$ npm start
```

Acessa o site: [http://localhost:3000](http://localhost:3000)

### Static Generation

``` bash
$ npm run generate
```

Estrutura do site vai está em `/dist`

## Licença

> [MIT License](https://gitlab.com/i.leonardo/template-nuxt-ts-subdomain/blob/master/LICENSE)

Copyright © 2019 [Leonardo C. Carvalho](https://gitlab.com/i.leonardo)
