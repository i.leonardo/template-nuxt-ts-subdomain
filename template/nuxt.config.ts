import NuxtConfiguration from '@nuxt/config';
import pkg from './package.json';

const config: NuxtConfiguration = {
  mode: 'universal',

  srcDir: 'src/',

  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
    ],
  },

  loading: { color: '#3b8070' },

  css: [],

  plugins: [],

  modules: [
    ['@nuxtjs/router', { keepDefaultRouter: true }],
  ],

  build: {
    postcss: {
      plugins: {
        autoprefixer: {},
      },
    },

    extend(_, { isClient, loaders: { vue } }) {
      if (isClient) {
        // eslint-disable-next-line no-param-reassign
        vue.transformAssetUrls = {};
      }
    },
  },
};

export default config;
