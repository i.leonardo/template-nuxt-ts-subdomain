import { RouteConfig } from 'vue-router';

const routeGer = (path: RouteConfig[], route?: string) => {
  if (route) {
    return path.filter(item => item.name!.split('-')[1] === route).map(item => ({
      ...item,
      path: item.path.split(`/subdomain/${route}`)[1] ? item.path.split(`/subdomain/${route}`)[1] : '/',
    }));
  }
  return path.filter(item => item.name!.indexOf('subdomain'));
};

export default routeGer;
