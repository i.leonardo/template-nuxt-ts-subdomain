import Vue from 'vue';
import Router from 'vue-router';

import routeGer from '@/utils/route';

Vue.use(Router);

// eslint-disable-next-line import/prefer-default-export
export function createRouter(ssrContext, createDefaultRouter) {
  const defaultRouter = createDefaultRouter(ssrContext);
  const path = defaultRouter.options.routes;
  let routes;

  if (process.server) {
    const { host } = ssrContext.req.headers;
    const parts = host.split('.');
    let lenght;

    if (parts[0] === 'www') lenght = 1;
    else lenght = 0;

    if (parts[lenght] === 'exemplo1') {
      routes = routeGer(path, 'exemplo1');
    } else if (parts[lenght] === 'exemplo2') {
      routes = routeGer(path, 'exemplo2');
    } else {
      routes = routeGer(path);
    }
  } else if (process.client) {
    const { host } = window.location;
    const parts = host.split('.');
    let lenght;

    if (parts[0] === 'www') lenght = 1;
    else lenght = 0;

    if (parts[lenght] === 'exemplo1') {
      routes = routeGer(path, 'exemplo1');
    } else if (parts[lenght] === 'exemplo2') {
      routes = routeGer(path, 'exemplo2');
    } else {
      routes = routeGer(path);
    }
  }

  return new Router({
    ...defaultRouter.options,
    routes,
  });
}
